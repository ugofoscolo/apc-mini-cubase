#!/bin/bash

REC=0
VOLUME=48
MON=8
MUTE=56
SOLO=48
READ=24
WRITE=16

for i in `seq 1 8`; do
	CHAN=$(($i-1))
	VOLUMET=$(($VOLUME+$i-1))
	MUTET=$(($MUTE+$i-1))
	SOLOT=$(($SOLO+$i-1))
	READT=$(($READ+$i-1))
	WRITET=$(($WRITE+$i-1))
	MONT=$(($MON+$i-1))
	RECT=$(($REC+$i-1))
       	echo "<ctrl><name>Volume${i}</name><stat>176</stat><chan>0</chan><addr>$VOLUMET</addr><max>127</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Volume${i}\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>volume</name><flags>0</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Mute${i}On</name><stat>144</stat><chan>0</chan><addr>$MUTET</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Mute${i}Off</name><stat>144</stat><chan>0</chan><addr>$MUTET</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Mute${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>mute</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Mute${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>mute</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Solo${i}On</name><stat>144</stat><chan>0</chan><addr>$SOLOT</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Solo${i}Off</name><stat>144</stat><chan>0</chan><addr>$SOLOT</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Solo${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>solo</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Solo${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>solo</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Read${i}On</name><stat>144</stat><chan>0</chan><addr>$READT</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Read${i}Off</name><stat>144</stat><chan>0</chan><addr>$READT</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Read${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>readEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Read${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>readEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Write${i}On</name><stat>144</stat><chan>0</chan><addr>$WRITET</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Write${i}Off</name><stat>144</stat><chan>0</chan><addr>$WRITET</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Write${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>writeEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Write${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>writeEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Mon${i}On</name><stat>144</stat><chan>0</chan><addr>$MONT</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Mon${i}Off</name><stat>144</stat><chan>0</chan><addr>$MONT</addr><max>5</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Mon${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>monitor</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Mon${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>monitor</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
        echo "<ctrl><name>Rec${i}On</name><stat>144</stat><chan>0</chan><addr>$RECT</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
        echo "<ctrl><name>Rec${i}Off</name><stat>144</stat><chan>0</chan><addr>$RECT</addr><max>3</max><flags>3</flags></ctrl>" >> dynamic-controllers.xml
	echo "<entry ctrl=\"Rec${i}On\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>recordEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
	echo "<entry ctrl=\"Rec${i}Off\"><value><device>VST Mixer</device><chan>$CHAN</chan><name>recordEnable</name><flags>256</flags></value></entry>" >> dynamic-entries.xml
done

cat header.xml > apc-mini.xml
cat static-controllers.xml >> apc-mini.xml
cat dynamic-controllers.xml >> apc-mini.xml
cat middle.xml >> apc-mini.xml
cat static-entries.xml >> apc-mini.xml
cat dynamic-entries.xml >> apc-mini.xml
cat footer.xml >> apc-mini.xml

rm dynamic-*.xml
